# projectframework
All of the components I use for any web app build.  
Just clone, then composer install, and I have a symfony project ready for me to start adding code.
Includes PHPUnit, Symfony2, and Compass.
JQuery, JQuery UI, Jquery Mobile, and AngularJS are connected to base template from google CDN
Bootstrap(SASS, no JS), UnderscoreJS, and BackboneJS are included in the AppBundle ready to go.
